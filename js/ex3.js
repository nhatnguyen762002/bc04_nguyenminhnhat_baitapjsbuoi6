function xuatKetQua() {
  var soValue = document.getElementById("txt-so").value * 1;
  var ketQuaEl = document.getElementById("txt-ket-qua");
  var sum = 1;

  for (var i = 1; i <= soValue; i++) {
    sum *= i;
  }
  ketQuaEl.classList.add("alert", "alert-dark");
  ketQuaEl.innerText = `Giai thừa của ${soValue} là: ${sum}`;
}
