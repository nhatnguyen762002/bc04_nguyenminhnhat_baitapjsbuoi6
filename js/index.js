function xuatKetQua() {
  var ketQuaEl = document.getElementById("txt-ket-qua");
  var count = 0;
  var sum = 0;

  for (var i = 1; sum <= 10000; i++) {
    count++;
    sum += count;
  }
  ketQuaEl.classList.add("alert", "alert-dark");
  ketQuaEl.innerText = `Số nguyên dương nhỏ nhất là: ${count}`;
}
