function xuatKetQua() {
  var ketQuaEl = document.getElementById("txt-ket-qua");
  var mauDo = "alert alert-danger";
  var mauXanh = "alert alert-primary";

  for (var i = 1; i <= 10; i++) {
    if (i % 2 != 0) {
      ketQuaEl.innerHTML += `<div class="${mauXanh}">div ${i} (lẻ)</div>`;
    } else {
      ketQuaEl.innerHTML += `<div class="${mauDo}">div ${i} (chẵn)</div>`;
    }
  }
}
