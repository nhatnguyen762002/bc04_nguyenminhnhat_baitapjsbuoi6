function xuatKetQua() {
  var xValue = document.getElementById("txt-x").value * 1;
  var nValue = document.getElementById("txt-n").value * 1;
  var ketQuaEl = document.getElementById("txt-ket-qua");
  var sum = 0;

  for (var i = 1; i <= nValue; i++) {
    sum += xValue ** i;
  }
  ketQuaEl.classList.add("alert", "alert-dark");
  ketQuaEl.innerText = `Tổng: ${sum}`;
}
